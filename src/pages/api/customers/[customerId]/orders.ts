import axios from 'axios';

export default async function handler(req: any, res: any) {
  const { customerId } = req.query;

  if (req.method === 'GET') {
    try {
      const response = await axios.get(`https://615f5fb4f7254d0017068109.mockapi.io/api/v1/customers/${customerId}/orders`);
      const data = response.data;
      res.status(200).json(data);
    } catch (error) {
      console.error(error);
      res.status(500).json({ message: 'Internal server error' });
    }
  } else {
    res.status(405).json({ message: 'Method not allowed' });
  }
}
