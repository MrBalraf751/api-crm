import { connectToDatabase } from "../../controller/dbConnection";
import apiConfig from "../../controller/rateLimit";
import { ParsedUrlQuery } from "querystring";
import { useRouter } from 'next/router';

export default async function handler(
  req: any,
  res: any
) {
  const { method } = req;

  apiConfig.limiter;

  switch (method) {
    case "POST":
      try {       

        const router = useRouter();
        const { customerId } = router.query;

        const { db } = await connectToDatabase();

        let result = await db.collection("customers").find({ id: customerId }).sort({}).toArray();
        
        return res.status(200).json(result);
      } catch (error) {
        console.error(error);
        res.status(500).json({ error: "Internal server error" });
      }
      break;

    default:
      res.status(405).json({ error: "Method not allowed" });
      break;
  }
}

function sanitize(query: ParsedUrlQuery): { id: string; order: string; } {
  throw new Error("Function not implemented.");
}
