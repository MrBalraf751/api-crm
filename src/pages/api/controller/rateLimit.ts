import rateLimit from "express-rate-limit";

const limiter = rateLimit({
  windowMs: 15 * 60 * 1000,
  max: 50,
  message: "Too many requests from this IP, please try again later.",
});

const apiConfig = {
  api: {
    bodyParser: {
      sizeLimit: "1mb",
    },
  },
  limiter,
};

export default apiConfig;